#include <stdio.h>
#include <string.h>
int htoi(char s[]);
int main(void)
{
	int n;
	char s[] = "0X2f2";

	// printf("'a':%d,'9':%d\n", 'a', '9');
	// printf("'9'-'0'=%d\n", '9' - '0');
	// printf("%d\n", 'f' - 'a');
	
	n = htoi(s);

	printf("n: %d\n", n);
	return 0;
}

int htoi(char s[]){
	int i = 0;
	int len = strlen(s);
	int n = 0;

	if(s[0] == '0' && s[1] == 'x' || s[1] == 'X'){
		i = 2;
	}

	for (; i < len; i++){
		//printf("i: %d\n", i);
		if(s[i] <= 'F' && s[i] >= 'A'){
			//printf("before: %c\n", s[i]);
			s[i] = s[i] - 'A' + 'a';
			//printf("after: %c\n", s[i]);
		}
		
		if (s[i]  >= 'a' && s[i] <= 'f'){
			n = n*16 + ( s[i] - 'a' + 10);
		}
		else{
			n = n * 16 + ( s[i] - '0' );
		}
	}

	return n;
}