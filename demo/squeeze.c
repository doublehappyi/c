#include <stdio.h>
#include <string.h>
void squeeze(char s[], char t[]);
//delete the element in char where index is index;
void deleteElement (char s[], int index);

int main(int argc, char const *argv[])
{
	int i ;
	char s[] = "abcdefzzdg";
	char t[] = "afgzd";
	//char r[];
	squeeze(s, t);

	printf("s: %s\n", s);
	return 0;
}

void squeeze(char s[], char t[]){
	int i; 
	int j;
	int s_len = strlen(s);
	int t_len = strlen(t);
	for (i = s_len; i >= 0; i--){
		for (j = 0; j < t_len; j++){
			if(s[i] == t[j]){
				deleteElement(s, i);
			}
		}
	}
}

void deleteElement(char s[], int index){
	int i;
	int s_len = strlen(s);
	for ( i = index; i < s_len - 1; i++){
		s[i] = s[i + 1];
	}
	s[s_len - 1] = '\0';
}