#include <stdio.h>
int main(int argc, char const *argv[])
{
	char s[10];
	int i  = 0;
	char c ;
	while (i < 10 && (c = getchar()) != '\n')
	{
		s[i] = c;
		printf("%c - ", c);
		i++;
	}
	printf("\n");
	printf("char: %s\n", s);
	return 0;
}