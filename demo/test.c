#include <stdio.h>
#include <string.h>
void log(char s[]);

int main(int argc, char const *argv[])
{
	char s[] = "ab";
	s[0] = '\0';
	log(s);
	printf("strlen(s): %d \n",strlen(s));
	return 0;
}

void log(char s[]){
	printf("%s\n", s);
}