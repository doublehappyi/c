#include <stdio.h>
#include <ctype.h>
char lower(char c);
int main(int argc, char const *argv[])
{
	char a = lower('A');
	char x = lower('1');
	printf("lower('A'): %c\n", a);
	printf("lower('100'): %c\n", x);

	printf("tolower('A')%c\n", tolower('A'));
	printf("isdigit('9')%d\n", isdigit('9') );
	return 0;
}

char lower(char c)
{
	if (c >= 'A' && c <= 'Z')
	{
		return c - 'A' + 'a';
	}
	else
	{
		return c;
	}
}