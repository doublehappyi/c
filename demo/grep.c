#include <stdio.h>
#include <string.h>

//if t exists in s, return index of s in t; else, return -1;
int strindex(char s[], char t[]);

int main(int argc, char const *argv[])
{
	char s[] = "abc";
	char t[] = "1234abcde";
	int i;

	i = strindex(s, t);

	printf("i: %d\n", i);
	return 0;
}

int strindex(char s[], char t[]){
	int i;
	int j;
	int s_len = strlen(s);
	int t_len = strlen(t);
	int flag;
	for(i = 0; i < t_len; i++){
		flag = 1;
		for(j = 0; j < s_len; j++){
			if(!(t[i + j] == s[j])){
				flag = 0;
			}
		}
		if(flag){
			return i;
		}
	}
	return -1;
}