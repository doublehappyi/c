//题目:古典问题:有一对兔子,从出生后第 3 个月起每个月都生一对兔子,小兔子长到第三个月后每
//个月又生一对兔子,假如兔子都不死,问每个月的兔子总数为多少?

#include <stdio.h>
int rabits(int month);
int main(int argc, char const *argv[])
{
	printf("rabits: %d", rabits(4));
	return 0;
}

int rabits(int month){
	int sum = 2;
	if(month > 2){
		sum = 2* rabits(month - 1) + 2;
	}

	return sum;
}