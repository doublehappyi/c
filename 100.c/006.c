//题目:用*号输出字母 金字塔 的图案。
/*
   *
  ***
 *****
*******

*/
#include <stdio.h>

void printTriangle(int row);
int main(int argc, char const *argv[])
{
	printTriangle(10);
	return 0;
}

void printTriangle(int row){
	int col = 2 * row - 1;

	for(int i = 1; i <= row; i++){
		int stars =  2 * i - 1;
		//printf("stars: %d\n", stars);
		for(int j = 0; j < col; j++){

			if( j >= (col - stars)/2 && j < (col + stars)/2 ){
				printf("*");
			}
			else{
				printf(" ");
			}
		}
		printf("\n");
	}
}