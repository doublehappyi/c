/*一个整数，它加上100后是一个完全平方数，再加上168又是一个完全平方数，请问该数是多少？*/
#include <stdio.h>

int main(int argc, char const *argv[])
{
	int x = 0;
	int start = 0, end = 1000;

	/*
	x+100 = i*i;
	x + 100 + 168 = j*j;
	
	j*j - i*i = 168;

	x = i*i - 100;
	*/

	int flag = 0;

	for(int i = start; i < end; i++ ){
		for( int j = start; j < end; j++){
			if( (j*j - i*i ) == 168 && (i*i - 100) > 0){
				flag = 1;

				printf("i== %d, j== %d\n", i, j);
				x = i*i - 100;
				break;
			}
		}

		if(flag){
			break;
		}

		if(i == end && end < 100000){
			end = end + 1000;
		}

		printf("(i: %d) ", i);
	}

	printf("x: %d\n", x);

	return 0;
}