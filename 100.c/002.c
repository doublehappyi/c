/*
企业发放的奖金根据利润提成。
利润(I)低于或等于10万元时，奖金可提10%；
利润高于10万元，低于20万元时，低于10万元的部分按10%提成，高于10万元的部分，可可提成7.5%；
20万到40万之间时，高于20万元的部分，可提成5%；4
0万到60万之间时高于40万元的部分，可提成3%；
60万到100万之间时，高于60万元的部分，可提成1.5%;
高于100万元时，超过100万元的部分按1%提成;
从键盘输入当月利润I，求应发放奖金总数？
*/
#include <stdio.h>

int main(int argc, char const *argv[])
{
	int size = 6;
	float profits[size] = {0.0, 10.0, 20.0, 40.0, 60.0, 100.0};
	float bonus[size] = {0.1, 0.075, 0.05, 0.03, 0.015, 0.01};
	
	
	while(1){
		float profit = 0.0;
		float b = 0.0;
		int flag = 0;
		printf("Pleas input your PROFIT:");
		scanf("%f", &profit);

		//printf("profit: %f\n", profit);

		for(int i = size - 1; i >= 0; --i){
			if( profit > profits[i] ){
				if(!flag){
					flag = 1;
					b = b + ( profit - profits[i] ) * bonus[i];
				}else{
					b = b + ( profits[i+1] - profits[i] ) * bonus[i];
				}
			}
		}

		printf("bonus: %f\n", b);
	}

	return 0;
}