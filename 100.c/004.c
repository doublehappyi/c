//输入某年某月某日,判断这一天是这一年的第几天?
#include <stdio.h>

int isLegalYear(int year);
int isLeapYear(int year);
int isLegalMonth(int month);
int isLegalDay(int year, int month, int day);

int countDay(int year, int month, int day);

int main(int argc, char const *argv[])
{
	while(1){
		int unsigned year, month, day;

		while(1){
			printf("Please enter the year: ");
			scanf("%d", &year);

			if(isLegalYear(year)){
				break;
			}
		}
		while(1){
			printf("\nPlease enter the month: ");
			scanf("%d", &month);

			if(isLegalMonth(month)){
				break;
			}
		}
		

		while(1){
			printf("\nPlease enter the day: ");
			scanf("%d", &day);
			
			if(isLegalDay(year, month, day)){
				break;
			}
		}

		printf("%d-%d-%d\n", year, month, day);
		printf("countDay: %d\n", countDay(year, month, day));

	}

	return 0;
}


int isLegalYear(int year){
	if(year > 0){
		return 1;
	}else{
		printf("\nError Year ! \n");
		return 0;
	}
}

int isLeapYear(int year){
	if(isLegalYear(year) && !(year % 4)){
		if(!(year % 100) && (year % 400)){
			return 0;
		}
		return 1;
	}else{
		return 0;
	}
}

int isLegalMonth(int month){
	if(month >=1 && month <= 12){
		return 1;
	}else{
		printf("\nError Month ! ");
		return 0;
	}
}

int isLegalDay(int year, int month, int day){
	int ret = 0;
	if(isLegalYear(year) && isLegalMonth(month) && day >=1 && day <= 31){
		if((month == 4 || month == 6 || month == 8 || month == 9 || month == 11) && day <= 30){
			ret = 1;
		}else if(month == 2 && ( (isLeapYear(year) && day <= 29) || (!isLeapYear(year) && day <= 28) )){
			ret = 1;
		}else if(month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12){
			ret = 1;
		}else{
			ret = 0;
		}
	}

	if(!ret){
		printf("\nError Day !");
	}

	return ret;
}

int countDay(int year, int month, int day){
	int ret = 0;
	int leap = isLeapYear(year);
	if(isLegalYear(year) && isLegalMonth(month) && isLegalDay(year, month, day)){
		switch(month){
			case 1:
				ret = day;
				break;
			case 2:
				ret = day + 31;
				break;
			case 3:
				ret = day + 31 + (28 + leap);
				break;
			case 4:
				ret = day + 31 + (28 + leap) + 30;
				break;
			case 5:
				ret = day + 31 + (28 + leap) + 30 + 31;
				break;
			case 6:
				ret = day + 31 + (28 + leap) + 30 + 31 + 30;
				break;
			case 7:
				ret = day + 31 + (28 + leap) + 30 + 31 + 30 + 31;
				break;
			case 8:
				ret = day + 31 + (28 + leap) + 30 + 31 + 30 + 31 + 31;
				break;
			case 9:
				ret = day + 31 + (28 + leap) + 30 + 31+ 30 + 31 + 31 + 30;
				break;
			case 10:
				ret = day + 31 + (28 + leap) + 30 + 31+ 30 + 31 + 31 + 30 + 31;
				break; 
			case 11:
				ret = day + 31 + (28 + leap) + 30 + 31+ 30 + 31 + 31 + 30 + 31 + 30;
				break;
			case 12:
				ret = day + 31 + (28 + leap) + 30 + 31+ 30 + 31 + 31 + 30 + 31 + 30 + 31;
				break;
			default:
				ret = 0;

		}
	}

	if(!ret){
		printf("Error year-month-day");
	}

	return ret;
}