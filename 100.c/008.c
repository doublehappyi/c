//题目:输出 9*9 口诀。
#include <stdio.h>
void printMulti();
int main(int argc, char const *argv[])
{
	printMulti();
	return 0;
}

void printMulti(){
	for (int i = 1; i <=9; i++){
		for(int j = 1; j <= i; j++){
			printf("%d * %d = %d  ", j, i, j*i);
		}
		printf("\n");
	}
}