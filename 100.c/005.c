//题目:输入三个整数 x,y,z,请把这三个数由小到大输出。
#include <stdio.h>

int main(int argc, char const *argv[])
{
	int size = 10;
	int arr[size] = {6,5,3,7,9,6,2,3,1,98};
	int temp;
	for(int i = 0; i < size; i++){
		for(int j = i; j < size; j++){
			if(arr[j] < arr[i]){
				temp = arr[i];
				arr[i] = arr[j];
				arr[j] = temp;
			}
		}
	}

	for(int i = 0; i < size; i++){
		printf("%d,", arr[i]);
	}

	printf("\n");
	return 0;
}