//有1、2、3、4个数字，能组成多少个互不相同且无重复数字的三位数？都是多少？
#include <stdio.h>
#include <string.h>

int main(int argc, char const *argv[])
{
	int len = 6;
	int count = 0;
	int i, j, k;
	for (i = 0; i < len; ++i)
	{
		for (j = 0; j < len; ++j)
		{
			if( j != i){
				for (k = 0; k < len; ++k)
				{
					if(k != i && k != j){
						++count;
					}
				}
			}
			
		}
	}

	printf("count: %d\n", count);
	return 0;
}